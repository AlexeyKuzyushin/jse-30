package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.entity.AbstractEntity;

public abstract class AbstractService <T extends AbstractEntity> implements IService<T> {

    @NotNull
    protected final IServiceLocator serviceLocator;

    protected AbstractService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
