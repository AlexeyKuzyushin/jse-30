package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.IAdminUserEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.UserDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class AdminUserEndpoint implements IAdminUserEndpoint {

    private IServiceLocator serviceLocator;

    public AdminUserEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public @NotNull List<UserDTO> findAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @WebMethod
    public void createUserWithEmail(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email) {
        serviceLocator.getUserService().create(login, password, email);
    }

    @Override
    @WebMethod
    public void createUserWithRole(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "role", partName = "role") @Nullable final Role role) {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getUserService().create(login, password, role);
    }

    @Override
    @WebMethod
    public void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login) {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login) {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login) {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id) {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getUserService().removeById(id);
    }
}
