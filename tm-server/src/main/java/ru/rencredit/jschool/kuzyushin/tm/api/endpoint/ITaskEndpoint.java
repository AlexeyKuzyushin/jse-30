package ru.rencredit.jschool.kuzyushin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import java.util.Date;
import java.util.List;

public interface ITaskEndpoint {

    Long countAllTasks(@Nullable SessionDTO session);

    @NotNull
    List<TaskDTO> findAllTasks(@Nullable SessionDTO sessionDTO);

    @NotNull
    List<TaskDTO> findAllTasksByUserId(@Nullable SessionDTO sessionDTO);

    @NotNull
    List<TaskDTO> findAllTasksByProjectId(@Nullable SessionDTO sessionDTO, @Nullable String projectId);

    void clearTasks(@Nullable SessionDTO sessionDTO) throws Exception;

    void createTask(@Nullable SessionDTO sessionDTO, @Nullable String projectId, @Nullable String name, @Nullable String description);

    @Nullable
    TaskDTO findTaskById(@Nullable SessionDTO sessionDTO, @Nullable String id);

    @Nullable
    TaskDTO findTaskByName(@Nullable SessionDTO sessionDTO, @Nullable String name);

    void removeTaskById(@Nullable SessionDTO sessionDTO, @Nullable String id);

    void removeTaskByName(@Nullable SessionDTO sessionDTO, @Nullable String name);

    void removeAllTasksByUserId(@Nullable SessionDTO sessionDTO);

    void removeAllTasksByProjectId(@Nullable SessionDTO sessionDTO, @Nullable String projectId);

    @NotNull
    Task updateTaskById(@Nullable SessionDTO sessionDTO, @Nullable String id,
                        @Nullable String name, @Nullable String description);

    void updateTaskStartDate(@Nullable SessionDTO sessionDTO, @Nullable String id, @Nullable Date date);

    void updateTaskFinishDate(@Nullable SessionDTO sessionDTO, @Nullable String id, @Nullable Date date);
}
