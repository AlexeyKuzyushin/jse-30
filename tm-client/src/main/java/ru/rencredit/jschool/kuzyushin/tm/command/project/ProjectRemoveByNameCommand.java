package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by name";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        if (serviceLocator != null) {
            @Nullable final String name = TerminalUtil.nextLine();
            @Nullable SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getProjectEndpoint().removeProjectByName(sessionDTO, name);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
