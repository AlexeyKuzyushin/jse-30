package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class ProjectViewByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-view-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        if (serviceLocator != null) {
            @Nullable final String id = TerminalUtil.nextLine();
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            @Nullable final ProjectDTO projectDTO = serviceLocator.getProjectEndpoint().findProjectById(sessionDTO, id);
            if (projectDTO == null) return;
            System.out.println("ID: " + projectDTO.getId());
            System.out.println("NAME: " + projectDTO.getName());
            System.out.println("DESCRIPTION: " + projectDTO.getDescription());
            System.out.println("[OK]");
        }
        else { System.out.println("[FAILED]"); }
    }
}
