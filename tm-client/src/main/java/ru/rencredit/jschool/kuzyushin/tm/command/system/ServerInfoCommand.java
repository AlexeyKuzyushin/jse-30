package ru.rencredit.jschool.kuzyushin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;


public class ServerInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "server-info";
    }

    @NotNull
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show information about connection port and host";
    }

    @Override
    public void execute() {
        System.out.println("[SERVER INFO]");
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        @NotNull final String host = serviceLocator.getSessionEndpoint().getServerHost(session);
        @NotNull final Integer port = serviceLocator.getSessionEndpoint().getServerPort(session);
        System.out.println("HOST: " + host);
        System.out.println("PORT: " + port);
        System.out.println("[OK]");
    }
}
